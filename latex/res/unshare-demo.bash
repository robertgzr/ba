$ cat /proc/self/cgroup
11:pids:/user.slice/user-1000.slice/session-42.scope
10:hugetlb:/
9:devices:/user.slice/user-1000.slice
8:freezer:/
7:net_cls,net_prio:/
6:perf_event:/
5:cpuset:/
4:memory:/user.slice/user-1000.slice
3:cpu,cpuacct:/user.slice/user-1000.slice
2:blkio:/user.slice/user-1000.slice
1:name=systemd:/user.slice/user-1000.slice/session-42.scope
0::/user.slice/user-1000.slice/session-42.scope
$ sudo unshare -fC /bin/bash
$ cat /proc/self/cgroup
11:pids:/
10:hugetlb:/
9:devices:/
8:freezer:/
7:net_cls,net_prio:/
6:perf_event:/
5:cpuset:/
4:memory:/
3:cpu,cpuacct:/
2:blkio:/
1:name=systemd:/
0::/
