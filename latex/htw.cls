% vim: filetype=tex
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{htw}[2016/10/09 Roberts HTW class]

\LoadClass[12pt,a4paper,twoside]{article}

\RequirePackage[margin=2.5cm]{geometry}
\RequirePackage{etoolbox}

\usepackage{perpage}
\MakePerPage{footnote}

% \RequirePackage{subfiles} % modular document
\setlength{\parindent}{0em}
\setlength{\parskip}{1.5em}
\renewcommand{\baselinestretch}{1.3}

\RequirePackage{ragged2e}
\RequirePackage{graphicx}
\RequirePackage{epstopdf}
\graphicspath{ {res/} }

\RequirePackage{float}
\RequirePackage{wrapfig}
\RequirePackage[font=footnotesize,labelfont=bf]{caption}
\RequirePackage{subcaption}
\RequirePackage{bookmark}

\RequirePackage{color}
\RequirePackage{xcolor}
\definecolor{htwgreen}{HTML}{76B900}
\definecolor{htwgray}{HTML}{575757}

%% Lang
\RequirePackage[quiet]{polyglossia}
\setdefaultlanguage[spelling=new]{german}
\setotherlanguage{english}

\RequirePackage[german=guillemets]{csquotes}

\RequirePackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    citecolor=htwgray,
    filecolor=blue,
    urlcolor=htwgray,
}

\RequirePackage{todo}
\renewcommand{\todomark}{\small\color{red}todo}


%% Font settings
\RequirePackage{fontspec} % font features
\defaultfontfeatures{Ligatures=TeX} % To support LaTeX quoting style
    \setmainfont{Noto Serif}
    \setsansfont{Noto Sans}
    \setmonofont{Iosevka Light}[CharacterWidth=Proportional]
    % \setmathfont[Digits,LatinhGreek]{}

%% Code
\RequirePackage{minted}
\setminted{
  fontsize=\footnotesize,
  frame=single,
  framesep=8pt,
  numbersep=4pt,
  baselinestretch=1.2,
  linenos
}
\usemintedstyle{colorful}

\renewcommand{\listoflistingscaption}{Quellcodezeichnis}
\renewcommand{\listingscaption}{Code}

% \RequirePackage{listings} % code hilighting
% \RequirePackage{listings-golang} % ^ add support for go code
% \lstset{
%   backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
%   basicstyle=\footnotesize,        % the size of the fonts that are used for the code
%   breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
%   breaklines=true,                 % sets automatic line breaking
%   captionpos=b,                    % sets the caption-position to bottom
%   escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
%   extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
%   frame=single,	                   % adds a frame around the code
%   keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
%   numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
%   numbersep=5pt,                   % how far the line-numbers are from the code
%   showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
%   showstringspaces=false,          % underline spaces within strings only
%   showtabs=false,                  % show tabs within strings adding particular underscores
%   stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
%   %title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
%   rulecolor=\color{black},
%   keywordstyle=\color{htwgreen},
%   commentstyle=\color{gray},
%   numberstyle=\tiny\color{gray},
%   stringstyle=\color{gray},
% }

\RequirePackage{multicol}

%% Custom commands
\newcommand\HTWBerlin{Hochschule für Technik und Wirtschaft Berlin}
% variables
% authors
\newcounter{cnt}
\newcommand\htwAddAuthor[2]{
    \stepcounter{cnt}
    \csdef{htw@author@first@\thecnt}{#1}
    \csdef{htw@author@last@\thecnt}{#2}
}
\newcommand\htwGetAuthor[1]{
    \csuse{htw@author@first@#1} \csuse{htwAuthor@last@#1}
}
\newcounter{rownum}
\newcommand\makeHtwAuthorList{
    \setcounter{rownum}{0}
    \whileboolexpr
        { test {\ifnumcomp{\value{rownum}}{<}{\thecnt}} }
        {
            \stepcounter{rownum}
            {\large
                \csuse{htw@author@first@\therownum}
                \textbf{\csuse{htw@author@last@\therownum}}
            }\\[0.3cm]
        }
}

\newcommand\htwCourse[1]{\def\@htw@course{#1}}
\newcommand\htwTopic[1]{\def\@htw@topic{#1}}
\newcommand\htwTitle[1]{\def\@htw@title{#1}}
\newcommand\htwSubtitle[1]{\def\@htw@subtitle{#1}}
\newcommand\htwDate{}
\newcommand\htwFooter{}
\newcommand\htwMakeProfs{}
\newcommand\htwMakeDates{}

\newcommand\htwRightOddHeader{\small \@htw@course}
\newcommand\htwRightEvenHeader{\small \HTWBerlin}
\newcommand\htwLeftOddHeader{\small \@htw@subtitle}
\newcommand\htwLeftEvenHeader{\small \@htw@title}

%% Headers
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\htwLeftEvenHeader}
\fancyhead[LO]{\htwLeftOddHeader}
\fancyhead[RE]{\htwRightEvenHeader}
\fancyhead[RO]{\htwRightOddHeader}

\fancyfoot{}
\newcommand\htwDefaultFooter{
  \fancyfoot[RE,LO]{\small \htwFooter}
  \fancyfoot[LE,RO]{\small \thepage} % \hspace{1pt} /\hspace{1pt} \pageref{LastPage}}
}
\htwDefaultFooter

\renewcommand{\headrulewidth}{0.4pt}
% \renewcommand{\footrulewidth}{0.4pt}

\renewcommand\maketitle{
    \begin{titlepage}
      \let\footnotesize\small
      \let\footnoterule\relax
      \let\footnote\thanks

      % titlepage
      \begin{multicols}{2}
        \begin{figure}[H]
            \includegraphics[trim= 7mm 2mm 7mm 4mm,clip,height=1.5cm]{htw.eps}
        \end{figure}
        \columnbreak
        \begin{figure}[H]
            \hfill
            \includegraphics[height=1.5cm]{ce.eps}
        \end{figure}
      \end{multicols}

      % \vskip 60\p@
      \begin{center}
        {\Large \@htw@topic}\\[0.5cm]
        \textsf{\textsc{\Large \@htw@course}}\\[1.5cm]
        \vfill
        {\Huge  \@htw@title}\\[0.5cm]
        {\large \@htw@subtitle}
      \end{center}

      % \vskip 1.5cm
      \null\vfill
      \begin{multicols}{2}
        % Multiple Authors
        \begin{flushleft}
            \makeHtwAuthorList
        \end{flushleft}

        \columnbreak
        
        \begin{flushright}
          \htwMakeProfs
        \end{flushright}
      \end{multicols}

      % \vfill
      % \begin{center}
      % {\large \@htw@date}\\[2cm]
      % \end{center}

      \htwMakeDates
    \end{titlepage}

    \setcounter{footnote}{0}
    \global\let\thanks\relax
    % \global\let\maketitle\relax
    \global\let\@thanks\@empty
    \global\let\@author\@empty
    \global\let\@date\@empty
    \global\let\@title\@empty
    \global\let\title\relax
    \global\let\author\relax
    \global\let\date\relax
    \global\let\and\relax
}
