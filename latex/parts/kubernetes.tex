\section{Kubernetes}\label{sec:kubernetes}

Kubernetes, oft k8s abgekürzt, ist ein System zum Betreiben und Skalieren von Applikationscontainern über viele Hostrechner. Nach der
Ankündigung im Zuge von Neuerungen der Container-Architektur von GCE\footnote{Google Compute Engine
  \url{https://en.wikipedia.org/wiki/Google_Compute_Engine}} im Juni 2014 \cite{google_update_2014} fand die Entwicklung
angeführt durch Google-Ingenieure auf GitHub als Open-Source-Projekt\footnote{Kubernetes Repositoy
  \url{https://github.com/kubernetes/kubernetes}} statt. Das Design und die Architektur basieren auf Erfahrungen aus
Googles eigener interner  Serverarchitektur und dessen Managementsystem, \emph{borg} \cite{oppenheimer_borg_2015}.

\begin{listing}[h]
  \inputminted{yaml}{res/kube-pod.yaml}
  \caption{Beschreibung eines nginx Pods}
  \label{lst:kube-pod-yaml}
\end{listing}

Die Grundidee von Kubernetes ist eine benutzerfreundliche Schnittstelle zu relativ komplexen Prozessen, wie
Ressourcenverteilung, Zuteilung von Rechnern aus dem Rechnerverbund und die Verwaltung von Containern und
deren Neuausführung im Störfall, zu schaffen. Realisiert werden diese Anforderungen durch eine domain-spezifische
Beschreibungssprache in YAML\footnote{YAML \url{https://en.wikipedia.org/wiki/YAML}} (bsp. Code~\ref{lst:kube-pod-yaml}), mit der ein angestrebter Cluster-Zustand definiert wird. Kubernetes stellt damit die Schritte
fest, die benötigt werden, um das System aus dem Ist-Zustand in den Zielzustand zu transferieren.

Beim Design wurde besonders darauf geachtet, dass Benutzer die Freiheit haben ihre eigenen Strukturen auf der Grundlage
von Kubernetes aufzubauen. So kommt das Projekt einem Betriebssystem für Rechnerverbünde nahe, auf dem
spezialisierte Einsatzszenarios möglich sind (siehe Zitat aus der CNCF-Charter in Abschnitt \ref{sec:operators}). Das abstrakte
Objekt des \emph{Service} erlaubt zusätzlich, ähnlich wie mit geteilten Prorammbibliotheken, die Wiederverwendbarkeit von Systemkomponenten.

\translatedblockquote{english}{burns_distributed_2015}{
  Just like the right way to code is the separation of concerns into modular objects, the right way to package applications in containers is the separation of concerns into modular containers.
}[(Übers.~\ref{tr:distributed})]

\subsection{Das Kubernetes Konzept}\label{sec:kubernetes-konzept}

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{kube_why_containers}
\caption{Schematischer Vgl. von Paketmanager/Container, \\\cite[Why Containers?]{kubernetes_authors_what_2017}}
\label{fig:containers}
\end{figure}

Die zentralen Objekte im Kubernetes-Ökosystem sind Container. Der Vorteil eine Infrastruktur aus Applikationscontainern
aufzubauen, liegt in der Isolation der Abhängikeiten von Software untereinander (z.~B. inkompatible Versionen einer Bibliothek) (siehe Abb.~\ref{fig:containers}). So bleibt die Laufzeitumgebung selbst im Fall von Betriebssystemupdates des
physikalischen Servers unangetastet.  Es besteht außerdem die Möglichkeit ein \emph{immutable} (dt. unveränderliches)
Betriebssystem \cite{fowler_immutableserver_2013} als Basis-OS im Container zu verwenden.  Solche Vorgehensweisen
minimieren die Angriffsoberfläche, sowie die Anfälligkeit für Regressionen. 

Die Forschung in diesem Feld umfasst z.~B. auch das Bereitstellen von Applikationen auf der reinen
Hypervisorarchitektur des Servers \cite{ballesteros_clive_2014}, indem der Softwarestack alle Funktionalitäten bereitstellt, die
gewöhnlicherweise vom OS übernommen werden (Unikernel\footnote{Unikernel \url{https://en.wikipedia.org/wiki/Unikernel}}-Design).

\subsubsection{Prozessvirtualisierung mit Containern}\label{sec:container-virt}

In einem Container hat der Prozess eine inidividuelle Sicht auf die Schnittstellen des Betriebssystems, etwa das Dateisystem, den Prozessbaum und die
Netzwerkschnittstellen.

In Linux basiert das Isolieren von Prozessen im Wesentlichen auf zwei Kernelfeatures: Namespaces und
cgroups\footnote{Diese Arbeit verwendet, außer am Satzanfang, die stylisierte Schreibweise mit kleinem \enquote{c}}\footnote{Details im Abschnitt~\ref{sec:cgroups}}.
Während Namespaces über einen \emph{syscall}\footnote{Syscall: system call \url{https://en.wikipedia.org/wiki/Linux_kernel_interfaces\#SCI}} zugänglich sind, erfolgt die
Interaktion mit den cgroups über ein Filesystem von \emph{special files}\footnotemark{}, überlicherweise in
\footnotetext{Special file \url{https://en.wikipedia.org/wiki/Synthetic_file_system}}
\texttt{/sys/fs/cgroup} (z.~B. Fedora).

Beim Starten eines Containers wird durch Versetzen des Prozesses in einen neuen Namespace die Ressourcenisolierung
erreicht, welche die Technologie so populär macht. Zum aktuellen \emph{Stable}-Release (v4.12) unterstützt Linux Namespaces
für:\\(vgl. \cite{kerrisk_namespaces7_2017})

\begin{itemize}
\tightlist
\begin{multicols}{2}
  \item cgroups
  \item IPC
  \item Netzwerk
  \item Mountpoints
\columnbreak
  \item PID
  \item Users
  \item UTS (Hostname, Domainname)
\end{multicols}
\end{itemize}

Wird einem Prozess ein neuer, unbenutzter Namespace zugeteilt, erscheint es aus dessen Sicht so, als wäre es der einzige
Nutzer der jeweiligen Ressource. Der Prozess kann nun mit der Ressource umgehen, als wäre sie Teil des originalen
Hostsystems, jedoch ohne die eigentliche Originalressource zu beeinflussen.

\begin{listing}[h]
  \inputminted{bash}{res/unshare-demo.bash}
  \caption{Neuer cgroup-Namespace mit \texttt{unshare}}
  \label{lst:unshare-ex}
\end{listing}

Das Prinzip wird mit dem Beispiel in Code~\ref{lst:unshare-ex}\footnote{Demo
  \url{https://asciinema.org/a/orltSDCdzNlVQ4Qa4MXcTX8ou}} mit dem \texttt{unshare}-Werkzeug demonstriert.  Hier
verlässt der Prozess (\texttt{/bin/bash}), den wir mit dem Tool durch Verwendung der Flag (\texttt{-f}) von unserer
Shell \emph{forken}, den intialen
cgroup-Namespace. Wie zu sehen ist, befindet sich der bash-Prozess vorher in diversen cgroups. Nach dem unshare zeigt der
Output von \texttt{cat /proc/self/cgroup} bei jedem Subsystem \texttt{:/}, die Wurzel der cgroup-Hirarchien.

\subsubsection{Pods}\label{sec:kube-pods}

Kubernetes führt zusätzlich das Konzept eines Pods ein. Hier definiert man eine Gruppe von
Containern, die logisch zusammen gehören, etwa einen Server und einen Prozess, der dessen Logs einsammelt und
persistiert.

Pods repräsentieren hierbei die kleinste Ressourceneinheit. Sie haben eine inidividuelle IP-Adresse im Cluster und bekommen,
abhängig von der Konfiguration, Systemressourcen, wie Arbeitsspeicher, CPU-Zeit und Speicherplatz zugewiesen (siehe Beispiel
Code~\ref{lst:kube-pod-yaml}).

Der Pod trägt ein Label (\texttt{app: frontend}, Zeile 6), um zu kennzeichnen, dass es
sich um Container mit dem Webseitencode handelt. Der hier definierte Pod besteht aus einem \texttt{nginx}\footnote{nginx \url{https://www.nginx.com/}}
(Webserver) Container und einem \texttt{git-monitor} Container, der kontinuerlich das Repositoy mit der Webseite
abfragt und im Fall von Änderungen die lokale Kopie erneuert. Da beide Container Zugriff auf dieselben Dateien
benötigen, wird in den Zeilen 24–25 ein flüchtiger Speicher deklariert, der von beiden Komponenten geteilt wird.

\subsection{Komponenten eines Kubernetes-Clusters}\label{sec:clusterkomponenten}

Um einen, wie in Code~\ref{lst:kube-pod-yaml} deklarierten, Soll-Zustand zu realisieren, benötigt Kubernetes die
Kontrollebene (\emph{control plane}). Diese besteht aus \emph{kube-apiserver}, \emph{kube-controller-manager} und
\emph{kube-scheduler} (auf dem Master-Node), sowie \emph{kubelet} und \emph{kube-proxy}, die von jedem weiteren Rechner
im Verbund (Worker-Nodes, auch Minions) ausgeführt werden \cite{kubernetes_authors_k8s-concepts_2017}.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{kube-arch}
\caption{Architekturdiagram, \\\cite{kubernetes_architecture_2017}}
\label{fig:kube-arch}
\end{figure}

\subsubsection{Der Master-Node}\label{sec:master-node}

Der Hauptrechner\footcite[Multi-master setups sind auch möglich,][]{kubernetes_authors_clusterfederation_2017} im
Clusterverbund, welcher \emph{Master} genannt wird, betreibt Komponenten, die Zustandsänderungen erkennen und in Auftrag geben.

Der \emph{kube-apiserver} hostet die API und erfüllt die Rolle eines Gateways (siehe Abb.~\ref{fig:kube-arch}) bei der Interaktion mit dem Cluster.
Sowohl Cluster-interne Komponenten und Prozesse, als auch Programme von außerhalb des Clusters, die dessen
Zustand abfragen bzw. modifizieren wollen, müssen
dieses durch Anfragen an den API-Server tun. Eine Aktion findet nicht ohne vorherige Validierung statt. So gelangen
fehlerhafte Daten, etwa unsinnige Betriebsanweisungen, nicht weiter in die Kubernetes-Maschinerie.
Dabei ist der  API-Server eine relativ simple Abstraktion des dahinter stehenden \emph{etcd}\footnote{etcd Repositoy \url{https://github.com/coreos/etcd}}-Clusters, einer
verteilten Key-Value Datenbank\footnote{Key-Value Datenbank \url{https://en.wikipedia.org/wiki/Key-value_database}}. Dieser kann in komplexeren Setups, wie auch Kubernetes selbst als verteiltes
System betrieben werden. Eine weitere Möglichkeit besteht seit November 2016 in Form der Einführung von
\emph{Operators}\footnote{Details in Abschnitt~\ref{sec:operators}} durch CoreOS \cite{deng_introducing_2016}.
Solche Betriebsmodelle werden als \emph{self hosted control plane} bezeichnet, da Kubernetes hier seine eigenen
Komponenten verwaltet.

\emph{Kube-controller-manager} und \emph{kube-scheduler} beobachten die API und den Clusterzustand und leiten Schritte
ein, um Zustandsübergänge durchzuführen. Wird z.~B. ein neuer Applikationscontainer angefordert oder fällt die Anzahl
der laufenden Replikas eines Containers unter die deklarierte Menge, muss der Scheduler einen Rechner (\emph{Worker
 -Node}) im Cluster finden, auf dem genug Ressourcen verfügbar sind und der mit den Regeln im Manifest des Containers
übereinstimmt. Wurde ein Ziel festgestellt, übergibt der Master den Befehl an das \emph{kubelet}, das auf dem
Worker läuft.

\subsubsection{Worker nodes}\label{sec:worker-nodes}

Jeder weitere Rechner im Kubernetes-System betreibt zumindest das \emph{kubelet} und einen \emph{kube-proxy}. Während
der Proxy die Netzwerkkommunikation zwischen Applikationen im Cluster ermöglicht, ist das \emph{kubelet} für das
Ausführen von Containern zuständig. Dabei ähnelt es dem Init-Prozess des Betriebssystems, denn es wacht über die
Zustände der Container und reagiert auf Befehle vom Master.

\subsection{Controller}\label{sec:kube-controller}

\translatedblockquote{english}[][Synopsis]{kubernetes_authors_kube-controller-manager_2017}{
  In applications of robotics and
  automation, a control loop is a non-terminating loop that regulates the state of the system. In Kubernetes, a
  controller is a control loop that watches the shared state of the cluster through the apiserver and makes changes
  attempting to move the current state towards the desired state.
}[(Übers.~\ref{tr:ctlr})]

Beim Planen einer Infrastruktur auf einem verteilten Rechnersystem sollen oft mehrere Kopien einer Applikation
betrieben und die Last zwischen den Instanzen verteilt werden. Um dieses Muster zu ermöglichen, hat Kubernetes
API-Objekte wie das \emph{Deployment}\footnote{Deployment \url{https://kubernetes.io/docs/concepts/workloads/controllers/deployment/}}, ein Beispiel für einen Controller.

Controller sorgen für das Einhalten eines aufgestellten Plans über wie viele Pods in welcher Konfiguration laufen
sollen. Die Konfiguration eines Pods in einem Controller hat nach dem Start keine direkten Auswirkungen mehr auf die
eigentliche Instanz. Dieses Verhalten ermöglicht etwa das schrittweise Ab- und Aufbauen von Komponenten bei Updates.


Für das Betreiben des Prototyp-Exporters war es nötig den Pod, der das Exporterprogramm enthält, auf jedem Rechnerknoten im Verbund auszuführen\footnote{siehe Abschnitt~\ref{sec:cgnet}}.
Dazu wird der \emph{DaemonSet} Controller verwendet.

Ein DaemonSet startet die deklarierten Pods für jeden, bzw. jeden (mit Labels o.~ä.) markierten, neuen Node im Cluster.

\translatedblockquote{english}[][What is a DaemonSet?]{kubernetes_authors_daemonsets_2017}{
  A DaemonSet ensures that all (or some) nodes run a copy of a
  pod.
}[(Übers.~\ref{tr:daemonset})]

\subsection{Exporter}\label{sec:kubernetes-exporter}

Wie an der Vielfalt von Lösungen für Monitoring-Setups für Kubernetes-Cluster zu erkennen ist, spielt dieser Teil der
Administration eine große Rolle im Ökosytem \cite{ismail_comparing_2015}.

Da Prometheus, wie eingangs erwähnt, ebenfalls Open-Source und das von der CNCF empfohlene
Werkzeug ist, entschieden wir uns mit den Exporter des Prototypen darauf aufzubauen.

Ein Exporter bedeutet in diesem Kontext ein zusätzliches Programm, das zusammen mit dem zu beobachtenden Container
ausgeführt wird. Es ist jedoch auch möglich das Exportieren der Laufzeitstatistiken, über die Client-Bibliotheken, direkt in die Applikation mit
einzubauen \cite{prometheus_authors_writing_2017}.

\subsubsection{Datenfluss zum Prometheus-Server}\label{sec:prometheus}

Prometheus fungiert gleichzeitig als Kollektor der Daten aus den einzelnen Exportern im gesamten Cluster, bzw. einer
logischen Untergruppe von Containern, und als persistente Datenbank für die Daten selbst. Dabei funktioniert das Sammeln
nach dem \emph{Pull-Modell} \cite{prometheus_authors_exposition_2017}, d.~h. Prometheus fragt bei den registrierten Exportern die Daten, die diese auf eine
vordefinierte Art und Weise bereitstellen, selbst ab. Zwar gibt es eine Schnittstelle, die auch ein \emph{Push-Modell}
unterstützt, doch ist der dokumentierte Verwendungszweck das Erfassen von Daten über kurzlebige Programme
\cite{prometheus_authors_pushing_2017}.

\subsection{Operatoren}\label{sec:operators}

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{coreos-prometheus-op}
\caption{Prometheus Operator Beziehungen, \\\cite[How it Works]{reinartz_prometheus_2016}}
\label{fig:arch-prom-op}
\end{figure}

Wie im Abschnitt~\ref{sec:master-node} beschrieben ermöglichen Kubernetes-Operatoren es, die
Administration von clusterweit agierende Komponenten, wie z.~B. Prometheus, mit Kubernetes-Primitiven zu automatisieren.
Das Beispiel-Einsatzszenario, dessen Konfiguration dem Prototypen beiliegt, erfordert eine einzelne Prometheus-Instanz.
Diese wird mit Hilfe des Prometheus-Operator\footnote{Prometheus-Operator
  \url{https://github.com/coreos/prometheus-operator}} betrieben, da reproduzierbare Tests damit einfacher fallen.

Als Mitglied der CNCF ist es eines der erklärten Ziele des Kubernetes-Projekts eine erweiterbare Schnittstelle zu
schaffen, mit der Lösungen für allgemeine sowie spezifische Bedürfnisse realisiert werden können.

\translatedblockquote{english}[CNCF Charter, Extensibility Model]{linux_foundation_cncf_2017}[(…)]{
  Any given sub-system should exist behind an open REST based API. Vendors should be able to provide alternate
  implementations of any given subsystem to optimize if for a given workload, or to add value through commercial
  technology. The models, and APIs, defined by the CNCF should be extensible such that vendors can add additional
  capabilities.
}[(Übers.~\ref{tr:cncf})]\label{quo:cncf-charter}

Die \emph{CustomResourceDefinition} (CRD), bis Version 1.8 auch \emph{ThirdPartyRessource}, erlaubt das Anlegen von neuen
API-Objekten \cite{kubernetes_authors_custom_2017}.
CoreOS definiert auf diesem Weg mit dem Prometheus-Operator neue Endpunkte zum Beschreiben einer Instanz des
Prometheus-Servers (oder einer Gruppe von redundanten Servern) auf dem gleichen Weg, wie in
Abschnitt~\ref{sec:kube-pods} beschrieben.
Hier arbeitet der Operator prinzipiell wie ein Controller, mit dem Unterschied, dass es sich bei den Objekten, die
kontrolliert werden, an Stelle von relativ überschaubaren Objekten wie Pods, um komplexere Delpoyment-Schemes handelt.

Zusätzlich zur Vereinfachung der Einrichtung von Prometheus wird ein weiteres API-Objekt, der
\emph{ServiceMonitor} definiert. Damit Prometheus selbstständig die Metriken der Exporter einsammeln kann, müssen diese
zuerst registriert werden. Der Operator orientiert sich dabei an den ServiceMonitor-Objekten, die notwendige
Parameter, z.~B. die IP-Adresse des Exporters, auslesen und bereitstellen können (siehe Abb.~\ref{fig:arch-prom-op}).

Die so erreichte Abstraktion erlaubt eine vollständige Automatisierung des Betriebs, wie auch bei nativen Kubernetes-Komponenten über die YAML-DSL.
