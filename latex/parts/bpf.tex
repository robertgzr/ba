\section{Berkley Package Filter}\label{sec:berkley-package-filter}

Die Benutzung von Unix Rechnern als Rückgrad unserer vernetzten Welt zeigt in welchem Grad wir von der Stabilität deren
Netzwerkarchitektur ausgehen. Das Aufrechterhalten dieser Strukturen involviert die Benutzung von Diagnose- und
Analysewerkzeugen, um gegen Probleme im Spektrum von fehlerhaften Konfigurationen bis hin zu Angriffen aus dem Netzwerk
selbst, effizient vorgehen zu können.

Diese Art von Systemwartung benötigt oft Zugang zu den Rohdaten des Netwerkverkehrs, bevor sie in den Protokollstack des Betriebssystems
gelangen.

\subsection{\texorpdfstring{\enquote{Classic}
BPF}{Classic BPF}}\label{sec:cbpf}

Als 1988 Forscher, unter anderen Van Jackobson und Steve McCanne, am U.C.~Berkley am ARPANET\footnote{ARPANET \url{https://en.wikipedia.org/wiki/ARPANET}}, dem
technologischen Vorgänger des heutigen Internets, arbeiten, begegnet ihnen das eingangs beschriebene Problem. Um an den
TCP/IP-Protokollen arbeiten zu können, fehlen performante Werkzeuge, die ein- bzw. ausgehende Netzwerkpakete filtern, um
sie analysieren zu können.

Die \emph{Network Research Group} des Lawrence Berkeley Laboratory (LBL)\footnote{Lawrence Berkley Lab. \url{http://www.lbl.gov/}}, bei der Jackobson und McCanne
Mitglieder sind, entwickelt dafür ein neues Kernel Modul, den Berkley Package Filter. Das Grundkonzept
besteht aus einem limitierten Befehlssatz zur Beschreibung von Paketfiltern, die zu Maschinencode kompiliert, in den
Kernel injeziert und dort verifiziert und ausgeführt werden.
Die Befehle werden von einer minimalen domain-spezifischen virtuellen Maschine entgegengenommen und machen so das
Ausführen von nahezu beliebigem Filtercode im Kernelspace möglich.
Die Filter setzen auf dem Treiberlevel an und filtern Pakete bereits vor dem protokollspezifischen
Kernelcode.

\clearpage
\subsubsection{Funktionsweise des Paketfilters}\label{sec:funktionsweise-des-paketfilters}

\translatedblockquote{english}{jacobson_bsd_1992}{ 
    Assuming one uses reasonable care in the design of the
    buffering model, it will be the dominant cost of packets you accept while the packet filter computation will be the
    dominant cost of packets you reject. Most applications of a packet capture facility reject far more packets than
    they accept and, thus, good performance of the packet filter is critical to good overall performance.
  }[(Übers.~\ref{tr:bpf-design})]

Die Forscher des LBL wählten aus diesem Grund einen \emph{Control Flow Graph (CFG)} als Ansatz zur Modellierung der
Entscheidung, ob ein Paket angenommen oder abgewiesen werden soll. Der Vorteil der Verwendung eines azyklischen CFG
liegt in der Effizienz mit der eine Register basierte Implementierung auf damals, wie heute verwendeten, ebenfalls
Register basierten RISC CPUs realisiert werden kann.

\begin{figure}[h]
\centering
    \includegraphics[height=8cm]{bpf_layout_diagram}
    \caption{Funktionsdiagram BPF \\\cite[2, Fig. 1]{jacobson_bsd_1992}}
    \label{fig:bpf-layout}
\end{figure}

Im Gegensatz zu einem Stack basierten Verfahren, bei dem ein Pointer emuliert werden muss und das die Paketannahme
durch einen Entscheidungsbaum darstellt, zeigt das CFG-Prinzip noch eine weitere vorteilhafte Eigenschaft. Die Knoten
stellen \emph{Zustände} des Filters, und damit des Pakets, dar, d.~h. wurde eine Bedingung abgearbeitet, kann der
folgende Verlauf des Graphen so entworfen werden, dass mehrmaliges Parsen des Paketheaders nicht nötig ist. Diese
Ersparnis wird vor allem relevant, wenn man bedenkt, dass das Design von Netwerkprotokollen stark auf deren Verkapselung
aufbaut. So muss für den Zugang zu einem Feld im Header eines TCP-Pakets stets auch der IP-Header angeschaut werden.

\begin{figure}[h]
\centering
    \includegraphics[height=0.3\textheight]{bpf_example_cfg}
    \caption{CFG-Filter: nach Herkunfts- o. Zielhost \emph{foo}, \\\cite[3.2.1 CFGs vs. Trees]{jacobson_bsd_1992}}
    \label{fig:bpf_example_cfg}
\end{figure}

Veranschaulichen lässt sich das an dem Beispiel in Abb.~\ref{fig:bpf_example_cfg} und
Abb.~\ref{fig:bpf_example_tree}. Der Filter soll alle Pakete, mit der Adresse \emph{foo} aus einem
Netzwerk mit IP, ARP und RARP (Reverse ARP) Protokollen herausfiltern. Abb.~\ref{fig:bpf_example_tree} zeigt, dass 6
Bool’sche Operationen notwendig sind um den Baumfilter zu durchlaufen, während der kürzeste Weg durch den CFG nur 2
Entscheidungen benötigt (Abb.~\ref{fig:bpf_example_cfg}).

\begin{figure}[h]
\centering
    \includegraphics[width=\textwidth]{bpf_example_tree}
    \caption{Baumfilter: nach Herkunfts- o. Zielhost \emph{foo}, \\\cite[3.2.1 CFGs vs. Trees]{jacobson_bsd_1992}}
    \label{fig:bpf_example_tree}
\end{figure}

Die Beschreibung des Filtergraphen erfolgt durch einen Satz von Operationen, deren Struktur Assembler-Syntax
nachempfungen ist. Realisiert wird das Filtern durch eine Zustandsmaschine mit einem Akkumulator- und einem
Indexregister, sowie einem limitierten zusätzlichen Speicherbereich.

\subsection{Extended BPF in Linux}\label{sec:ebpf}

In der im vorangehenden Abschnitt beschriebenen Form existierte die virtuelle Maschine für BPF\footnote{in folgenden Abschnitten wird für die
    pre-3.18 Variante von BPF in Linux die Abkürzung \emph{cBPF} (classic
    BPF) verwendet} auch im Linux-Kernel bis zu
ihrer grundlegenden Revision mit dem Release von Linux 3.18, welches die Einführung des \texttt{bpf(2)} syscalls
markiert \cite[1.3]{kernelnewbies.org_linux3.18_2014}.

Die neue Architektur der BPF-Maschine, \emph{extended BPF (eBPF)} genannt, brachte neue Features, die das Ausführen von
BPF Programmen nicht nur performanter, sondern auch flexibler anwendbar machen. Das Verwenden eines JIT\footnote{JIT = just-in-time
    \url{https://en.wikipedia.org/wiki/Just-in-time_compilation}}-Compilers ermöglicht es den Pseudomaschinencode zur Laufzeit
  in plattformspezifische\footnote{verfügbar auf x86\_64, SPARC, PowerPC, ARM, ARM64, MIPS, s390 \cite[JIT compiler, L462]{starovoitov_filter_2017}} Instruktionen umzuwandeln.
Zusätzlich können eBPF-Programme 10 64-bit breite Register mit beliebigem Speicherzugriff, anstelle des Load/Store-Stacks in
cBPF nutzen \cite[Folie 22]{starovoitov_bpf_2015}.

Es bleiben die Kernbeschränkungen, die auch schon für cBPF-Programme gültig waren. Ein einzelnes Programm ist auf 4096 Instruktionen limitiert.
Es dürfen keine Schleifen erzeugt werden. Im Gegensatz zu cBPF erlaubt eBPF hier Backward-Jumps, da der Kernel-interne Verifier
erkennt, ob ein Programm ordnungsgemäß terminiert \cite[eBPF Architecture]{borkmann_getting_2016}\cite[eBPF verifier,
L1004]{starovoitov_filter_2017}.

\translatedblockquote{english}{borkmann_getting_2016}{
  [The BPF virtual machine] operates event driven on inputs (\enquote{context}), which in case of \texttt{tc} is a \texttt{skb}, for example, traversing ingress or egress direction the traffic control layer of a given device.
}[(Übers.~\ref{tr:eventdriven})]

Die wichtigste Neuerung für den Kontext dieser Arbeit ist die Design-Entscheidung, das Ausführen von eBPF-Programmen als
Reaktion auf generische Events aus dem Kernel zu erlauben. Es stehen verschiedene Programmtypen zur Auswahl die jeweils
an unterschiedliche Events angehangen werden können und über den neu eingeführten \texttt{call()} BPF-Opcode Hilfsfunktionen
aus Kernel je nach Typ des jeweiligen Programms aufgerufen werden können \cite{kerrisk_bpf2_2016}.

Zum Release von Linux 4.12 sind die folgenden Programmtypen unterstützt
\cite[\texttt{/include/linux/bpf\_types.h}]{torvalds_linux_2017}:

\begin{itemize}
\tightlist
\begin{multicols}{2}
    \item Socket Filter
    \item \texttt{tc} Classifier/Actor
    \item eXpress Data Path (XDP)
    \item Cgroup Socket Buffer (skb), Socket
\columnbreak
    \item Lightweight Tunnel (lwt) In/Out/Xmit
    \item Kprobe
    \item Tracepoint
    \item Perf Event
\end{multicols}
\end{itemize}

\subsubsection{BPF-Maps}\label{sec:bpf-maps}

Ein weiter wichtiger Bestandteil von eBPF sind \emph{Maps}. Beim Einsatz von eBPF-Programmen zum Filtern von
Netzwerkverkehr oder ganz besonders zum Tracen von Kernelevents ist Kontext zu vorangegangen Programmabläufen nötig.
Außerdem fehlt in Hinsicht auf das Tracing sonst eine Möglichkeit die gewonnenen Daten aus dem Kernel zu extrahieren.

Maps bieten einen effizienten Key-Value-Speicher, der im Kernelspace liegt und über den \texttt{bpf(2)} syscall auch aus
dem Userspace zugänglich ist. Es sind diverse Typen von Maps verfügbar, die sich überwiegend durch den Typ des Mapkeys
unterscheiden (aktuelle Liste \cite[\texttt{/docs/kernel-versions.md}, Tables aka Maps]{iovisor_bcc_2017}).

\subsubsection{Schreiben von eBPF-Programmen}

Das Schreiben von eBPF Programmen in eBPF-Assembler (\emph{bpf\_asm}) ist zwar möglich, doch in der Praxis eher unüblich, da
LLVM\footnote{LLVM \url{http://llvm.org/}} ein
Compilerbackend für den BPF-Befehlssatz aus einen speziellen Subset der C-Programmiersprache erlaubt. Es ist somit möglich, \emph{clang}
als Compiler zu verwenden.

\begin{listing}[H]
\begin{minted}{shell}
clang -D__KERNEL__ -D__ASM_SYSREG_H \
    -Wno-unused-value -Wno-pointer-sign -Wno-compare-distinct-pointer-types \
    -fno-stack-protector \
    -O2 -emit-llvm -c "src/cgnet.c" \
    -o - | llc -march=bpf -filetype=obj -o "out/cgnet.o"
\end{minted}
\caption{Befehl zum compilen von BPF C-Code zu einem ELF mit clang}
  \label{lst:bpf-cgnet-clang}
\end{listing}

\subsubsection{Die gobpf-Bibliothek}

Für die Arbeit am Prototyp war es nötig die Interaktion mit dem \texttt{bpf(2)} syscall von der Go-Programmiersprache
aus durchzuführen. Das heißt alles, von der Installation und dem Anhängen des BPF-Programms an die cgroup bis zum
Auslesen der Daten aus der Kernel-Map, muss von Go aus zugänglich sein.
Als Systemprogrammiersprache stehen uns hier direkte Schnittstellen zu den syscalls zur Verfügung\footnote{Go
    \texttt{syscall} pkg \url{https://godoc.org/syscall}}. Alternativ könnte man die Verwendung des syscalls selbst
auch in C-Programme auslagern und diese dann über das \emph{cgo}\footnote{cgo \url{https://golang.org/cmd/cgo/}}-Interface verwenden.
Beide Methoden kommen in der \emph{gobpf}-Bibliothek\footnote{gobpf \url{https://github.com/iovisor/gobpf}} von Iovisor zur Anwendung.

Die eigentlichen BPF Programme werden hier immer noch in dem C-Subset geschrieben. Bei der Verwendung kommen nun zwei
Ansätze in Frage. Man kann mit einem von clang erzeugten ELF-File, das den BPF-Bytecode enthält, direkt arbeiten
oder die Existenz der \emph{BPF Compiler Collection}\footnote{bcc \url{https://github.com/iovisor/bcc}} (BCC) auf dem
Zielsystem voraussetzen und den BPF-Code zur Laufzeit erst compilen lassen.

Für den Prototypen entschieden wir uns für den ersten Ansatz, da unser BPF-Program vorher bekannte Parameter hat und
deshalb die Flexibilität des letzteren Ansatzes nicht notwendig ist.
