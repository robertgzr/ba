\section{Linux Control Groups}\label{sec:cgroups}

Als Systemadministrator eines Multiuser-Systems ist man oft in der Situation die begrenzten Ressourcen des Rechners,
wie z.~B. Anteil an der CPU-Zeit und dem Arbeitspeicher, nicht immer nur gerecht zu verteilen, sondern bestimmten
Prozessen oder Prozessgruppen einen prozentual höheren Anteil zukommen zu lassen. In der Welt von Docker und Kubernetes
bzw. von Prozessvirtualisierung wird diese Kontrolle auch relevant, da man hier nun Containern bestimmte
Ressourcen zuweisen und die Benutzung anderer gänzlich sperren will. 

\begin{listing}[h]
  \inputminted{shell}{res/cgtree.txt}
  \caption{Gekürzter cgroup-Baum eines ARMv7-Systems mit \emph{ArchLinux}}
  \label{lst:cgtree}
\end{listing}

Der Linux-Kernel stellt zu diesem Zweck eine Gruppe von \emph{low-level} Features, die \emph{control groups (cgroups)}
genannt werden, bereit. Docker und Kubernetes greifen darauf zurück, sowie grundlegende Bestandteile der meisten modernen
Distrutionen, etwa \emph{systemd}\footnote{systemd Repository \url{https://github.com/systemd/systemd}}, das sie
neben den o.~g. Anwendungen auch zur Prozessorganisation verwendet. Code~\ref{lst:cgtree} zeigt die Prozessegruppen des
Systems (\texttt{.scope}, \texttt{.slice} und \texttt{.service}), an denen dann z.~B. eine Ressource limitiert werden kann.
Für eine kurze Demonstration wird in Code~\ref{lst:cgnew} die Benutzerschnittstelle des Kernels direkt mit primitiven
Kommandozeilen-Programmen angesprochen.

Cgroups wurden mit Linux~2.6.24 \cite[2.10 Task Control Groups]{kernelnewbies.org_linux2.6.24_2008} eingeführt und erhielten seitdem eine grundlegende
Revision in Form von \emph{cgroups version 2} in Linux 4.5 \cite[1.8. cgroup unified hierarchy is considered stable]{kernelnewbies.org_linux4.5_2016}. Die Benutzung von
cgroups v1 kam bei der Planung des Prototyps nicht infrage, da die Schnittstelle im eBPF-System nur das Anhängen von
Programmen an cgroupsv2 erlaubt.
\\Aus diesem Grund konzentriert sich die Beschreibung hier auf v2.

\subsection{Ressourcenverwaltung}\label{sec:cg-res}

In seinem Kern ist die Implementiereung von cgroups nichts weiter als die Möglichkeit Prozesse (und in Version 1
Threads) zu Gruppen zusammenzufassen. Das Setzen von Limits und das Auslesen der aktuellen Systembelastung wird von \emph{Subsystemen} übernommen. 
Jedes Subsystem kontrolliert über einen \emph{controller} jeweils eine Ressource bzw. einen Teil einer
Ressource.
\\Zum Release von Linux 4.12 sind folgende Controller verfügbar:
\\\cite{heo_cgroupv2_2015}

\begin{itemize}
\tightlist
  \item Memory
  \item I/O
  \item PID
  \item RDMA (\emph{Remote Direct Memory Access})
  \item Perf Events (zum Tracen von Kernel-Funktionen)
\end{itemize}
\emph{Der CPU-Controller ist in v4.12 noch nicht gemerged, jedoch in \cite{heo_cgroupv2_2015} ausgiebig dokumentiert}

Da Docker und damit de fakto auch Kubernetes noch mit dem cgroupv1-System
arbeiten\footnote{\url{https://github.com/opencontainers/runc/issues/654}}, ist es momentan erforderlich die zu
beobachtenden Prozesse selbst in die cgroups2 Hirarchie zu bewegen.

Bei der Automatisierung dieses Vorgangs im Zuge des Exporters muss besonders auf die Beziehungen zwischen den Prozessen
im Hinblick auf ihre Containerisierung geachtet werden. Details zu der Implementierung finden sich in Abschnitt~\ref{sec:cgnet-probleme}.

Der Umgang mit cgroups wird im folgeneden Code~\ref{lst:cgnew} und Code~\ref{lst:cgmove} demonstriert.
Zuerst legen wir eine neue cgroup an, dies wird in Zeile 1 durch Erstellen eines neuen Ordners im cgroupfs-Root
\texttt{/sys/fs/cgroup/unified/} erreicht.

\begin{listing}[h]
  \inputminted{shell}{res/cgnew.txt}
  \caption{Inspektion einer neu angelegten cgroup}
  \label{lst:cgnew}
\end{listing}

Linux legt für uns die Kernkontrolldateien an:
\begin{itemize}
\tightlist
  \item {\makebox[4cm][l] {\texttt{.controllers}} führt die verfügbaren Ressourcen-Controller}
  \item {\makebox[4cm][l] {\texttt{.events}} benutzt zur Kommunikation von Zustandsänderungen}
  \item {\makebox[4cm][l] {\texttt{.procs}} bietet Kontrolle über die zugehörigen Prozesse}
  \item {\makebox[4cm][l] {\texttt{.subtree\_control}} bietet Kontrolle über die aktivierten Ressourcen-Controller}
\end{itemize}
  
Wie in Code~\ref{lst:cgnew} zu sehen, besteht keine Möglichkeit die Ressourcenverwaltung von cgroupsv2 für die neue Gruppe
\texttt{test/} zu nutzen, da der Output von Zeile 10 keine verfügbaren Controller auflistet. 
Das Testsystem hier hat für die Ressourceverwaltung nur cgroupsv1 aktiviert.
Um trotzdem nur die Organisation von cgroupsv2 zu nutzen, wird der Prozess in Code~\ref{lst:cgmove} durch Schreiben seiner PID
in die \texttt{cgroup.procs} Datei in die entsprechende Hirarchie versetzt.

\begin{listing}[h]
  \inputminted{shell}{res/cgmove.txt}
  \caption{Bewegen eines Prozesses in eine cgroup}
  \label{lst:cgmove}
\end{listing}

Die zusätzliche PID (Code~\ref{lst:cgmove}, Zeile 5) gehört dem \texttt{tee} Prozess, da beim Bewegen eines Prozesses in
eine cgroup alle seine Child-Prozesse mitbewegt werden.
