\section{Der Prototyp - cgnet}\label{sec:cgnet}

Wie eingangs beschrieben war das Ziel der Arbeit einen Prototypen zum Sammeln von Netzwerkstatistiken auf Basis der
Zugehörigkeit von Prozessen zu Pods (cgroups) zu entwickeln.
Die gewonnenen Daten sollen einem Prometheus-Server zugänglich gemacht werden.

Die Durchführung des Projekts begann am 5. Juli.\footnote{
    Die Angaben beziehen sich auf den Code im Repository\\
    \url{https://github.com/kinvolk/cgnet/tree/e193f748829a7fefe3d2b6944717e43862a7f5ae} oder\\
    \url{https://gnzler.io/~thesis}
}.

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{cgnet_arch}
\caption{Architektur des Exporters}
\label{fig:cgnet-arch}
\end{figure}

\subsection{Umsetzung des Prototyps}\label{sec:cgnet-umsetzung}

Zur Realisierung des Projekts war es notwendig drei Technologien miteinander zu verknüpfen. Die folgenden Abschnitte
dokumentieren die Herangehensweise und Probleme geordnet nach System.

\subsubsection{Das BPF-Programm}\label{sec:cgnet-bpf}

Der Code für das BPF-Programm, das die Daten sammelt, befindet sich unter \texttt{/bpf/src/cgnet.c} im
Repository. Wir definieren eine Map für die Daten und eine Funktion, die für jedes empfangene Paket ausgeführt wird.

Das BPF-Programm für den Prototyp erfüllt einen sehr simplen Zweck.
Da es auf das Eintreffen eines neuen Netzwerkpakets registriert wird, zählen wir diese und speichern die Anzahl und die
Gesamtgröße aller Pakete in Bytes, nachdem Pakete, die nicht an Port 80 ausgehen, herausgefiltert sind.

\begin{listing}
    \begin{minted}{C}
__u16 dest = 0;
bpf_skb_load_bytes( skb, sizeof(struct iphdr)       /*  assume IP header   */
                    offsetof(struct tcphdr, dest),  /*  assume TCP header  */
                    &dest, sizeof(dest));

if (dest == __constant_ntohs(80))
    *bytes += skb->len;
    \end{minted}
    \caption{Zugriff auf das Zielport-Feld im Socket Buffer (\texttt{skb}) des TCP-Pakets}
    \label{lst:bpf-access-skb}
\end{listing}

Die Daten werden in einer simplen Map vom Typ \texttt{BPF\_MAP\_TYPE\_ARRAY} unter den fest definierten Keys 0 und
1 gespeichert. 

Für das Abfragen der Map aus dem Userspace wurde eine Kontrollstruktur für das BPF-Programm geschrieben
(\texttt{/bpf/controller.go}), die das Programm, unter Verwendung der \emph{gobpf}-Bibliothek, an die spezifizierte cgroup anhängt und fortlaufend die Map
abfragt.

\begin{listing}
    \begin{minted}{Go}
for {
    select {
    case <-ctx.Done():
            return
    case <-c.quit:
            return
    case <-time.After(1 * time.Second):
            packets, _ := lookup(c.module, packetsKey)
            _ := c.packetsHandler(packets)

            // ...
    }
}
    \end{minted}
    \caption{Ausschnitt aus dem Mainloop des BPF-Controllers (ohne Fehlerbehandlung)}
    \label{lst:bpf-ctl-run}
\end{listing}

Neue Werte werden auf Go-Channels geschickt und der Benutzer kann individuelle Handler für die Datenpunkte definieren.
Die Architektur des Controllers und des Pakets um das BPF-Programm wurde im Hinblick auf eine paralelle Verwendung des
Codes geschrieben.
So erlaubt das \emph{for-select}-Pattern (siehe Code \ref{lst:bpf-ctl-run}) in Go es koninuierlich auf das zuerst eintreffende Event zu reagieren. 
Im Fall von \emph{cgnet} sind die ersten beiden Events unterschiedliche Fälle des Programmabbruchs, während die Kernlogik einmal pro Sekunde
ausgeführt wird.

\subsubsection{Interaktion mit der k8s-API}\label{sec:cgnet-k8s}

Zum Exportieren von Statistiken über einzelene Kubernetes-Pods erfordert es Kenntnis vom aktuellen Clusterzustand.
Zumindest werden Informationen über die startenden und stoppenden Pods benötigt, um zu wissen, wo das BPF-Programm
angehängt werden muss.
Wie in Abschnitt \ref{sec:kubernetes} beschrieben, ist die universelle Quelle dafür die Kubernetes-API.

Ein Grund für die Wahl der Go-Programmiersprache für das Projekt war die gute Integration mit k8s in Form eines API-Client\footnote{k8s API-Client in Go \url{http://godoc.org/k8s.io/client-go}}.
Da Kubernetes selbst in Go geschrieben ist, können die selben Source-Code-Objekte verwendet werden.

Die API stellt einen \emph{ResourceInformer} zur Verfügung, der Funktionen für alle relevanten Fälle im Hinblick auf
Pods im Cluster akzeptiert (Add, Update, Delete) und diese für jedes Event ausführt.
Wir starten eine Instanz des BPF-Controllers als Reaktion auf einen neuen Pod und beenden diesen, sobald das entsprechende
\emph{Delete}-Event empfangen wird.

Die gesamte Konfiguration des Exporters erfolgt über das \enquote{all-in-one} Manifest
(\texttt{/manifests/deploy/all-in-one.yaml}), welches aus den Einzelmanifesten für die benötigten Komponenten
zusammengesetzt ist.
Kernstück dabei ist der \emph{DaemonSet}-Controller (siehe Abschnitt \ref{sec:kube-controller}) in
\texttt{/manifests/deploy/cgnet-exporter-ds.yaml}, damit alle Nodes eine Kopie des Programms betreiben.

Eine Herausforderung stellt nun die Aufteilung der Aufgaben auf die einzelen Kopien des Exporters dar. Eine
Implementierung mit \enquote{intelligenter} Vernetzung der Replikas untereinander wäre unnötig komplex. 
Deshalb muss sichergestellt werden, dass ein Controller nur BPF-Programme für Pods auf dem lokalen Node installiert. Da das Anhängen eines BPF-Programms direkt mit
dem Kernel interagiert, wäre es auch nicht möglich dies für andere Nodes zu tun.
Die Liste der Pods, die wir mit dem \emph{Add}-Event erhalten, muss also nach Nodes gefiltert werden.

\subsubsection{Exporter-Client für Prometheus}\label{sec:cgnet-prom}

Prometheus erwartet die Daten in strukturierter Form als simple Textwebseite auf einem vorher mit dem Server
registrierten Port. Die offizielle Client-Bibliothek, die uns in Go zur Verfügung steht, bietet uns eine einfache Abstraktion dafür, indem wir eine Struktur mit den Daten erstellen und diese mit dem
eingebauten HTTP Server registrieren.

Das größte Problem stellt hierbei die Umgebung und die Registration mit dem Prometheus Server dar.
Generell ist beim Instrumentieren z.~B. einens Webservers die IP-Adresse bekannt und diese kann registriert werden.
In dem Fall hier wissen wir nur, dass das Exporter-Programm auf jedem Rechenknoten im Cluster läuft, jedoch nicht
welche IP diese haben. Zusätzlich wird für jeden neuen Node eine neue Kopie des Exporters gestartet, die sich wiederum mit
Prometheus registrieren muss, damit die Daten auch da aufgefasst werden.
Der Prometheus-Operator bietet eine intuitive Lösung mit dem \emph{ServiceMonitor}. 
Dafür müssen alle Replikas des Exporters mit einem Selektor-Label gestartet werden, damit sie gefunden werden können. Der \emph{ServiceMonitor} Servers
konfiguriert dann den Prometheus-Server mit den erkannten IPs.

\subsection{Probleme}\label{sec:cgnet-probleme}

Zwar benutzt Kubernetes, bzw. Docker, cgroups zur Ressourcenkontrolle, doch verwenden beide noch cgroupsv1, da dort mehr
Controller zur Verfügung stehen.
Für die Verwendung des \texttt{BPF\_PROG\_TYPE\_CGROUP\_SKB} ist es notwendig die Prozesse in der cgroupv2 Hirarchie zu haben.
Als temporäre Lösung ist es möglich die relevanten Prozesse per Hand in eine eigene Struktur unter
\texttt{/sys/fs/cgroup/unified} zu bringen.

Wie bereits in Abschn. \ref{sec:cg-res} erwähnt, ist die Integration von Version 2 in \emph{runc}, die Kernkomponente von Docker (der
meistbenutzten Container-Engine in Kubernetes) bereits ein Thema, jedoch warten die Entwickler noch auf weitere wichtige
Features, um nicht zwei Lösungen nebeneinander unterstützen zu müssen.

Bis dahin ist der Prototyp im wesentlichen ein \emph{proof-of-concept} für das Verwenden von BPF zum Sammeln von
Netzwerkverkehrsdaten gruppiert nach cgroups.

\subsection{Erkenntnisse}\label{sec:cgnet-erkenntnisse}

Kubernetes und BPF sind zwei Technologien, die an unterschiedlichen Enden des Spektrums der Abstraktion in Software
liegen. Während man in BPF direkt mit den Byte-Offsets in Netzwerkpaketen arbeiten muss, um an relevante Informationen zu
kommen, ist im Kubernetes-Universum alles eine Struktur, deren Eigenschaften direkten Einfluss auf komplexe
Konfigurationen einer Gruppe von vernetzten Rechnern haben.

Diese Arbeit will zeigen, wie es möglich ist mit Hilfe dieser \emph{low-level} Werkzeuge eine alltägliche Anforderung an
moderne, verteilte Rechnersysteme zu lösen und dabei performant, idiomatisch und einfach einsetzbar zu sein.
