DOC=main.pdf
TEX_PARTDIR=latex/parts

MD=$(wildcard markd/*.md)
TEX=$(subst markd,$(TEX_PARTDIR),$(MD:.md=.tex))

# PANDOC vars
PANDOC_BIN=$(shell which pandoc)
PANDOC_FMT=html
PANDOC_OUT=$(notdir $(MD:.md=.$(PANDOC_FMT)))

.PHONY: mk tex md clean docker pandoc crowbook

all: md tex

mk: $(DOC)
	@make -C latex/ latexmk

tex: $(DOC)
$(DOC): $(TEX)
	@make -C latex/ xelatex
	mv latex/main.pdf $@

md: $(TEX)
$(TEX): $(MD)
	mkdir -p latex/parts
	pandoc -f markdown -t latex -o $@ markd/$(notdir $*).md
clean-md-renders:
	rm -rf $(TEX_PARTDIR)

pandoc: $(PANDOC_OUT)
$(PANDOC_OUT): $(MD)
	mkdir -p $(PANDOC_FMT)
	pandoc -f markdown -t $(PANDOC_FMT) -o $(PANDOC_FMT)/$@ $<

crowbook:
	languagetool-server -p 7878 &>/dev/null &
	crowbook markd/crow.book
	pkill java

clean:
	rm -rf $(DOC)
	@make -C latex/ dist-clean

docker:
	docker run --rm -i --user="$(id -u):$(id -g)" -v $(pwd)/latex:/data blang/latex make xelatex
